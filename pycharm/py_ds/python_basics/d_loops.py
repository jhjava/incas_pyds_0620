#############################################
########### Loops ###########################
#### only two kinds of loops: while, for-each
#############################################

## 1) for each loop to iterate over a container
numbers = [ 5, 6, 2, 55, 6, 3]

# for i, el in enumerate(numbers):
#     print(i, el)

#for el in numbers:
#    print(el)

## 2) for-loop
###         0  1  2   3  4  5
numbers = [ 5, 6, 2, 55, 6, 3]

for i in range(len(numbers)):
     print(i, numbers[i])

######        start  end(excl.)  step
print(list(range(3, len(numbers), 2)))

##########################
### while-loop ###########
##########################
###         0  1  2   3  4  5
numbers = [ 5, 6, 2, 55, 6, 3]

i = 0
while i < len(numbers):
    print(i, numbers[i])
    i += 1

### ending a loop from within: break
print('================')
numbers = [ 5, 6, 2, 55, 6, 3]
for el in numbers:
    if el==2:
        print('element found')
        break     #### ends a loop from within
    print(el)

#####################################
numbers = [ 5., 6., 2., 55., 6, 3]
mean = 4.5
std = 1.2
for i, el in enumerate(numbers):
    numbers[i] = (el - mean)/std
print(numbers)

var = 'hallo es ist fünfzehn uhr'
print(var[:5])