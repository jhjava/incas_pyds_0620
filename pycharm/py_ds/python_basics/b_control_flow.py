######################
#####################
## Control Flow

### if
name = 'Eva'
var = name == 'Paul'
print(var)
if name == 'Paul':
    print('hello Paul')
    print('****')

print('after if clause')

### if / else
name = 'Eva'
if name == 'Paul':
    print('Paul')
else:
    print('other name')

### if / elif / else
name = 'Eva'
if name == 'Paul':
    print('Paul')
elif name == 'Eva':
    print('Hello Eva')
elif name == 'Martin':
    print('Hello Marin')
else:
    print('other name')

### Comparision Operators

## == (equal) != (unequal)
## > (greater) >= (greater equal) < (less) <= (less equal)
print( 5 != 4)
print('a' > 'b')

### Logical Operators
### Short ciruit
result = 5 < 4 and 5 == 4
print(result)

result = 5 > 4 or 5 == 4
print(result)

### not-operator
result = not 5 > 4 or 5 == 4
print(result)

#######################
### Arithmetic operators
## +, -, *, /, ** (Base ** Exponent), % (Modulo)
## Increment and decrement: +=, -=
var = 0
var -= 2 #var = var - 2
var -= 3 #var = var - 3
print(var)

