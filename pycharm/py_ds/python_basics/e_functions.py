################################
############### Functions ######
################################
### def name(arguments):
def add_two_values(num1: int, num2: int):
    #assert isinstance(num1, int) and isinstance(num2, int), 'num1 or num2 is not an integer'
    result = num1 + num2
    return result

print(add_two_values)

result = add_two_values('x', 'y')
print(result)

formula = '5 + 6'
print(eval(formula))

def useless_function():
    print('hello this is a useless function')
    return 1  ### end a function, and you can return values to the caller of a function
    print('hello again -  this is still a useless function')

print(useless_function())


########### default-argument values
### Alt + Shift + E --> execute selection on console
#### Ctrl + Shift + F12 ---> new shortcut (you find shortcuts in file/settings/keymap
def add_two_or_three_values(value1, value2, value3=0):
    result = value1 + value2 + value3
    return result

result = add_two_or_three_values(4, 5, 9)
print(result)

#### function with a lot of default values
def a_lot_of_default_values(a=0, b=1, c=True, d='hello', e=8, f=1, g=False):
    print('a=', a, 'b=', b, 'c=', c, 'd=', d, 'e=', e, 'f=', f, 'g=', g)


params = { 'a':4, 'c':6, 'd':3, 'b':True, 'e':0, 'f':0, 'g':0 }
### dictionaries can be used to set arguments in a function
a_lot_of_default_values(**params)
a_lot_of_default_values()
a_lot_of_default_values(a=4, b=5)
a_lot_of_default_values(4, g=True)
a_lot_of_default_values(4, 1, True, 'hello', 8, 1, True)

### functins with multiple return values
def add_two_values(value1, value2, return_values=False):
    if return_values:
        result = value1 + value2
        return result, value1, value2  ## a tuple is produced automatically out of values
    return value1 + value2

print(add_two_values(5, 6, False))

result, value1, value2 = add_two_values(5, 6, True)
print(result, value1, value2)

###########################################################
def does_not_return_anything():
    print('hello in no return')

var = does_not_return_anything()
print(var) ### var == None
