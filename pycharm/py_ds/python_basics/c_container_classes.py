################################
### Container-Classes ##########
################################
#### lists, tuple, sets, dictionaries (key-value)

##############################
### 1) list ##################: are not type-save

## index   0    1     2       3
list_1 = [ 5, 6.11, 'text', True ]
print(type(list_1))
print(list_1)

### receive objekts from list
first = list_1[0]
last = list_1[3]
print('first', first, 'last', last)

### exchange elements in a list
list_1[2] = 'hello'
print(list_1)

## index   0    1     2       3
list_1 = [ 5, 6.11, 'text', True ]
## var = list_1[4]
## list_1[4] = 'XXX'

### add Elements: method: append
list_1.append('XXX')
print(list_1, list_1[4])

### remove Elements from a list: del / remove
del(list_1[1])  ### directly access element in list by index pos
print(list_1, list_1[1])

list_1 = [ 5, 6.11, 'text', True, 'text' ]
print(list_1)
list_1.remove('text')   ### has to search for the element to delete
                        ### deletes only first occurence of element
print(list_1)
#list_1.remove('ABC')

###################################################
### check if element is in list, with "in" operator
if 'ABC' in list_1:
    list_1.remove('ABC')

### Indexing
###        0   1      2       3      4
list_1 = [ 5, 6.11, 'text', True, 'text' ]

### check the length of a list: len
num_el_in_list = len(list_1)
print(num_el_in_list)

print( list_1[len(list_1)-1] ) ### last index position: length of list - 1 !!!

###        0   1      2       3      4
###       -5  -4     -3      -2    -1    ### reverse indexing
list_1 = [ 5, 6.11, 'text', True, 'text' ]

print(list_1[-1], list_1[-5])

#################################
############ Slicing ############
##         0    1    2       3      4
list_1 = [ 5, 6.11, 'text', True, 'text' ]
list_sub = [list_1[2], list_1[3]]

## Slicing start_index (incl.) : end_index (excl.)
list_sub = list_1[2:4]
list_sub = list_1[2:]
list_sub = list_1[:3]
print(list_sub)

## Slicing with steps:

numbers = [1, 2, 3, 4, 5, 6, 7, 8]
number_sub = numbers[::2]
number_sub = numbers[::-1]
### slicing: start(incl.):end(excl.):step
number_sub = numbers[4:0:-1]
print(number_sub)

##############################
numbers = [1, 2, 3, 4, 5, 6, 7, 8]
copy_numbers = numbers
copy_numbers2 = numbers[:]
copy_numbers3 = numbers.copy()

print(copy_numbers)
numbers[2] = 'XX'
print(numbers)
print(copy_numbers)
print(copy_numbers2)
print(copy_numbers3)

list_of_lists = [ ['Paul', 'Schickentanz'],
                  ['Eva', 'Teller'] ]
print(list_of_lists)
copy1 = list_of_lists.copy()
list_of_lists[1][0] = 'Martina'
print(list_of_lists)
print(copy1)

#####
############               0                     1
###########          0          1             0        1
list_of_lists = [ ['Paul', 'Schickentanz'], ['Eva', 'Teller'] ]
list_of_lists[0].remove('Paul')
print(list_of_lists)

####################################################
####################################################
#### tuples : tuples are lists but you cannot change them
#### order of elements have a meaning
##          0       1,      2
tuple_1 = (577, 'Paul', 'Schickentanz')
print(tuple_1[1])
# tuple_1[1] = 'XXX'
customer_id, first_name, surname = tuple_1
print(customer_id, first_name, surname)

### cast a tuple as a list
list_1 = list(tuple_1)
print(list_1)

tuple_1 = (577, 'Paul', 'Schickentanz', 4, 5, 6, 6, 8)
customer_id, first_name, *throw_away = tuple_1
print(customer_id, first_name, throw_away)

#######################################
############ Sets #####################

### Sets can save only unique values
### Sets don't use index positions to organize elements but a hashing syste
### Sets you would use if you are oftenly have to lookup for 'customers', numbers

names = ['Eva', 'Paul', 'Maria', 'Paul', 'Eva']
names_set = set(names)
print(names_set)
#print(names_set[0])

'Paul' in names_set   ## Works much faster because of hashing system!!
names_set.add('Martina')
print(names_set)

#############################
###### Dictionaries #########
### key : value
#############################
##              key(index):value
engl_ger_dict = {'flower':'Blume', 'window':'Fenster', 'house': 'Haus'}
print( engl_ger_dict['window'])

engl_ger_dict['screen'] = 'Bildschirm'
engl_ger_dict['house'] = 'Gebäude'   ### if a key is already in the dict, it will be overwritten
print(engl_ger_dict)

sql = {'customer_id': 345, 'first_name':'Paul', 'surename':'Schickentanz'}
print(sql['first_name'])

#### retrieve all keys out of a dictinary
list_keys = engl_ger_dict.keys()
list_values = engl_ger_dict.values()
print(list_keys)
print(list_values)

if 'guitar' in engl_ger_dict:
    engl_ger_dict['guitar']




