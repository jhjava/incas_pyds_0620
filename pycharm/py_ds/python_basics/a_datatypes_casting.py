
### Execute code: Ctrl + Shift + F10
# print('hello python')

####################################
### Datatypes: there are no primitive datatypes in python, everthing is an object
### Simulated primitive datatypes: int (integers), float (float), str (string),
### bool (boolean)

### int
var = 10
print(var)
print( type(var) )

### float
var = 11.1
print(var)
print( type(var) )

### Strings
var = 'hello'
print(var)
print( type(var) )

### booleans (bool)
var = True
var = False
print(var)
print( type(var) )

### Styleguide ####
### references variables and function should always start with a lower character
## Var = True ##
customer_id = 11 ## use underscores to differentiate between words
### a means customer id
customer_id = 5
age_customer = 8.9

#### Casting --> transfer one data type into another data type
### cast an int as a float:
var = 5
print(type(var))
var = float(var)  ### casting var as a float
print(type(var))
print(var)

### cast float as int
var = 5.67
print(type(var))
var = int(var)
print(type(var))
print(var)

### cast int to str
var = 5
var = str(var)
print(type(var))

### cast str to int / float
var = '67'  ### produced a str
print(type(var))
print(var)
var = int(var)
print(type(var))
print(var)

### castings into boolean
### every value is casted as --> True
### except for: 0, '', None  --> False
var = -89
var = 'text'
var = None
var = bool(var)
print(var)

### Execute Selection in Interactive Console: Alt + Shift + E