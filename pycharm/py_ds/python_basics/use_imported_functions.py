import utils.arithmetics
import utils.arithmetics as util
from utils.arithmetics import add_values, subtract_values

print('name of use_imported...', __name__)
print(util.add_values(9, 9))

### call a static function
print(add_values(7, 8))

list_a = ['A', 'B']
list_a.append('C')

var = utils.arithmetics.add_values(5, 6)
print(var)
var = utils.arithmetics.subtract_values(5, 6)