
###### Lambdas are simple functions where the algorithm fits in one line

def add_two_values(value1, value2):
    return value1 + value2

var = add_two_values(4, 5)
print(var)

### same function as lambda
add_two_values_lambda = lambda v1, v2: v1 + v2 ### return v1 + v2
print(add_two_values_lambda)
result = add_two_values_lambda(5, 6)
print('lambda result:', result)


#### a function that transforms all values of a column
def subtract_value_from_values_in_list(a_list, var):
    b_list = []
    for el in a_list:
        result = el - var
        b_list.append(result)
    return b_list

def add_value_to_values_in_list(a_list, var):
    b_list = []
    for el in a_list:
        result = el + var
        b_list.append(result)
    return b_list

numbers = [5, 3, 5, 2, 66, 22]
normalized_data = subtract_value_from_values_in_list(numbers, 11)
print(normalized_data)

##### Meta-Function
def transform_values_in_list(a_list, function):
    b_list = []
    for el in a_list:
        result = function(el)
        b_list.append(result)
    return b_list

numbers = [5, 3, 5, 2, 66, 22]
def mini_function(x, mean_x=11, std_x=1.2):
    result = (x - mean_x)/std_x
    return result
def subt_function(x):
    return x - 2

standardized = transform_values_in_list(numbers, mini_function)
standardized = transform_values_in_list(numbers, subt_function)
standardized = transform_values_in_list(numbers, lambda x: x - 2)
print(standardized)


def decide_a_b(a, b):
    if a > b:
        return a
    else:
        return b

decide_lambda = lambda a, b: a if a > b else b   ### if/else-branch in one line
print('decide_lambda', decide_lambda(a=5, b=4))

### inline if: create an if/else statement in one line
a = 5
b = 6
c = a if a > b else b

print(c)



######### Explanation of zip-function

list_a = ['A', 'B', 'C', 'D']
list_b = [5,  6,  7,  8]

for el_a, el_b in zip(list_a, list_b):
    print(el_a, el_b)