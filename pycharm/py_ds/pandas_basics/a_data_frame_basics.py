import pandas as pd

sql = [(778, 'Paul', 'Schickentanz' ),
       (111, 'Eva', 'Teller'),
       (654, 'Michael', 'Heinrichs')]
print(sql)

df = pd.DataFrame(sql)
df.columns = ['customer_id', 'fname', 'sname']
print(df)

### Select a column from dataframe:
### variante 1
series_col =  df['fname']   ### series object: has only one column without a name
print(series_col)
## variante 2
series_col = df.fname  ### treat column as attribute of dataframe
print(series_col)

### select a rows from dataframe
### select all persons with customer_id < 300
dfs = df[ df['customer_id'] < 300 ]
print(dfs)

### check the type of a column
print(df.customer_id.dtype)

print(df)
np_array_df = df.values
print(np_array_df)
### slicing in arrays: start_row:end_row , start_col:end_col
print(np_array_df[:2, 1:])
