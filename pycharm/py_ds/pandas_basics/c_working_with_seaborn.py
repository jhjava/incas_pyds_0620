from os.path import join
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'insurance.csv'

pd.set_option('display.max_columns', 20)
pd.set_option('display.float_format', lambda x: '%.3f' % x)

df = pd.read_csv( join(path, file), sep=',', header=0 )
print(df.head(5))

#### Charts #####
### 1) Histogram
sns.distplot(df['bmi'], bins=10)
plt.show()  ### shows the conatainer in extra window

### 2) Relational Plot / Scatter Plot
sns.relplot(data=df, x='age', y='charges', hue='smoker')  ### categorical coloring of dots
df.bmi.describe()
sns.relplot(data=df, x='age', y='charges', hue='bmi')   ### heatmap if hue is a float

sns.relplot(data=df, x='age', y='charges', hue='smoker', kind='line', ci=99)  ### categorical coloring of dots
plt.xlabel(rotate=45)

### 3) Barplots
sns.set_palette('pastel')
sns.set_context('paper')    ### paper, notebook, talk, poster
sns.catplot(data=df, x='smoker', y='charges', kind='bar' )   ### mean values over charges for smoker/non-smoker
plt.ylim(5000, 30000)
plt.show()

### 4) Stackplot: regions by smoker/nonsmokers
crosstab = pd.crosstab(df['region'], df['smoker'], normalize='index')
crosstab['sum'] = crosstab['no'] + crosstab['yes']
print(crosstab)
sns.barplot(data=crosstab, x=crosstab.index, y='sum', color='blue')
sns.barplot(data=crosstab, x=crosstab.index, y='yes', color='red')
plt.show()