import pandas as pd
from os.path import join
# text = 'hello \n text goes on....'
# print(text)

path = r'C:\Users\jhjav\git\incas_pyds_0620\data'  ### raw string
file = 'insurance.csv'
## path_to_file = r'C:\Users\jhjav\git\incas_pyds_0620\data\insurance.csv'
print(join(path, file))  ### concatenate path and file-string

### 1) read a CSV-file as a DataFrame using the read_csv-Method
###    read_csv returns an Objekt of type pandas.DataFrame

df = pd.read_csv(join(path, file), sep=',', header=0)
print(df.head())  ### head: show first rows in DataFrame

### 2) Select data from a DataFrame
series = df['age']   ### selects one column, returns a Series Object
dfc = df[['age', 'region', 'bmi']]  ### select multiple columns for df
print(dfc.head())

#### drop: Select all variables except ...
dfd = df.drop(['age'], axis=1 )
print(dfd)


### 3) Describe variables
des = df[['age', 'bmi']].describe()
print(des)
print(des.index, des.columns)

#### selected parameters: mean, std, min, max etc.
mean_age, mean_std = df['age'].mean(), df['age'].std()
print(mean_age, mean_std)
print(df[['age', 'bmi']].median().values )

### frequency table // relative percentages (normalize)
freq = df['region'].value_counts(normalize=True)
print(freq)

### crosstab // relative percentages: normalize 'index', 'columns'
crosstab = pd.crosstab(df['region'], df['smoker'], normalize='index')
crosstab = pd.crosstab(df['region'], df['smoker'], normalize='columns')
print(crosstab)

### mean table: mean value for age over regions
mean_table = df.groupby(['region', 'smoker'])[['age', 'children']].mean()
print(mean_table)

######################
### 4) Transform
#####################
print(df.head())
### 1) Variante 1
df['charges_10m'] = df['charges'] / 10000
print(df.head())

### 2) Using map-method to transform a column
## a) lambda
df['charges_10m_l'] = df['charges'].map(lambda x: x / 10000 )
print(df.head())
### recode sex: female = 1, male=0
df['sex_d'] = df['sex'].map(lambda x: 0 if x=='male' else 1)
print(df.head())
print(pd.crosstab(df['sex'], df['sex_d']))

## b) dictionary: keys (original values): values (newly created values)
recode = {'female':1, 'male':0}
df['sex_d'] = df['sex'].map(recode)
print(df.head())
