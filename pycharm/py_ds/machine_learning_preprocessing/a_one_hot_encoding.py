from os.path import join
import pandas as pd
import numpy as np

pd.set_option('display.max_columns', 20)
pd.set_option('display.precision', 3)
np.set_printoptions(precision=3, suppress=True)

## a) CSV-Datei als DataFrame laden
path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'insurance.csv'

df = pd.read_csv(join(path, file))
print(df.head())

#### demonstrate dummy variables in regression
X = df['smoker'].map(lambda x: 1 if x=='yes' else 0)
X = X.values.reshape(-1, 1)
print(X.shape)
print(X[:5])

from sklearn.linear_model import LinearRegression
linear = LinearRegression()
linear.fit(X, df['charges'])
print(linear.intercept_, linear.coef_)

#### One Hot Encoder
from sklearn.preprocessing import OneHotEncoder

X_ohe = df[['sex', 'smoker', 'region']]
ohe = OneHotEncoder(sparse=False)
ohe.fit(X_ohe)   ### train
print(ohe.categories_)
X_ohe = ohe.transform(X_ohe)  ### train and test (test only transform)
X_ohe[:5]

### DataFrame out of one hot encoding set
# cols = []
# for el in ohe.categories_:
#     cols.extend(el)
# print(cols)
# df_ohe = pd.DataFrame(X_ohe, columns=cols)
# df_ohe.head()
np.set_printoptions(precision=3, suppress=True)
linear.fit(X_ohe, df['charges'])
print(linear.coef_)
print(linear.score(X_ohe, df['charges']))