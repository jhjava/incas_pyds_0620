import joblib
from os.path import join
import numpy as np

def load_preprocessing_classes(path: str, files: list):
    instances = []
    for file in files:
        instances.append(joblib.load(join(path, file)))
    return tuple(instances)

def preprocess_data(X_pred:dict, ohe, scaler):
    X_ohe = [[ X_pred['sex'], X_pred['smoker'], X_pred['region'] ]]
    X_ohe = ohe.transform(X_ohe)
    X_std = [[X_pred['age'], X_pred['bmi'], X_pred['children']]]
    X_std = scaler.transform(X_std)
    X_compl = np.concatenate([X_ohe, X_std], axis=1)
    return X_compl

def produce_predictions(X_pred, model):
    return model.predict(X_pred)

if __name__ == '__main__':
    path = r'C:\Users\jhjav\git\incas_pyds_0620\pycharm\py_ds'
    files = ['model_insurance.pkl', 'ohe_insurance.pkl', 'scaler_insurance.pkl']
    model, ohe, scaler = load_preprocessing_classes(path, files)
    X_pred = {'sex':'female', 'smoker':'yes', 'region':'southwest',
              'age': 46, 'bmi':26, 'children':1}
    X_pred = preprocess_data(X_pred, ohe, scaler)
    y_pred = produce_predictions(X_pred, model)
    print(y_pred)
