from os.path import join
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.metrics import mean_absolute_error
from sklearn.linear_model import LinearRegression

def load_data_separate_train_test(path_to_file: str, X_cols: list, y_col: str, test_size=0.2):
    df = pd.read_csv(path_to_file)
    X = df[X_cols]
    y = df[y_col]
    return train_test_split(X, y, test_size=test_size)

def preprocess_train_test(X_train, X_test,
                          ohe_cols=['sex', 'smoker', 'region'],
                          std_cols=['age', 'bmi', 'children']):
    ### 1. Step: One Hot Encode: fit train data / transform train / test data
    X_train_ohe = X_train[ohe_cols]
    X_test_ohe = X_test[ohe_cols]
    ohe = OneHotEncoder(sparse=False, handle_unknown='ignore')
    ohe.fit(X_train_ohe)
    X_train_ohe = ohe.transform(X_train_ohe)
    X_test_ohe = ohe.transform(X_test_ohe)

    ### 2. Step: Standard: fit train data / transform train / test data
    X_train_std = X_train[std_cols]
    X_test_std = X_test[std_cols]
    scaler = StandardScaler()
    scaler.fit(X_train_std)  ## calculates mean and standard deviation
    X_train_std = scaler.transform(X_train_std)
    X_test_std = scaler.transform(X_test_std)

    ### 3. Step: merge OneHotEncoding Matrix and the Standardized Matrix
    X_train_compl = np.concatenate([X_train_ohe, X_train_std], axis=1)
    X_test_compl = np.concatenate([X_test_ohe, X_test_std], axis=1)

    return X_train_compl, X_test_compl, ohe, scaler

# d) Select and train model on train partition, Evaluate model on test validation partition
def train_evaluate_linear_regression(X_train, y_train, X_test, y_test, mae=True):
    linear = LinearRegression()
    linear.fit(X_train, y_train)
    r_square = linear.score(X_test, y_test)
    y_pred_test = linear.predict(X_test)
    mae = mean_absolute_error(y_test, y_pred_test)
    if mae:
        return (r_square, mae), linear
    return r_square, linear

# f) Save best model and preprocessing classes
def save_model_and_preprocessing_classes(classes:list, names=list):
    import joblib
    for class_, name_ in zip(classes, names):
        joblib.dump(value=class_, filename=name_)
    return True

if __name__ == '__main__':
    path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
    file = 'insurance.csv'
    X_train, X_test, y_train, y_test = load_data_separate_train_test(
        join(path, file), X_cols=['age', 'bmi', 'children', 'sex', 'smoker', 'region'],
        y_col='charges')
    X_train, X_test, ohe, scaler = preprocess_train_test(X_train, X_test)
    metrics, model = train_evaluate_linear_regression(X_train, y_train, X_test, y_test)
    print(metrics)
    save_model_and_preprocessing_classes([model, ohe, scaler],
                                         ['model_insurance.pkl', 'ohe_insurance.pkl',
                                          'scaler_insurance.pkl'])

