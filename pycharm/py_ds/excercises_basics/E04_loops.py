########### Loops

## a)
buying_list = ['tomatoes', 'basil', 'bread', 'mozzarella', 'oil', 'vinegar']

### b)
for el in buying_list:
    if el=='mozzarella':
        print('mozzarella in list')

### c) reverse characters of ingredients
reverse_list = []
for el in buying_list:
    reverse_el = el[::-1]
    reverse_list.append(reverse_el)
print(reverse_list)