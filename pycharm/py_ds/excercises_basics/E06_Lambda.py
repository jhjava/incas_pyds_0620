def compare_and_keep(list1, list2, function):
    return_list = []
    for el1, el2 in zip(list1, list2):
        keep = function(el1, el2)
        return_list.append(keep)
    return return_list

list1 = [4, 5, 6, 1, 2]
list2 = [8, 1, 3, 4, -1]

def a_function(x1, x2):
    if x1 > x2:
        return x1
    else:
        return x2

### with normal function
result = compare_and_keep(list1, list2, a_function)
print(result)
### with lambda
result_l = compare_and_keep(list1, list2, lambda x, y: x if x > y else y)
print(result_l)