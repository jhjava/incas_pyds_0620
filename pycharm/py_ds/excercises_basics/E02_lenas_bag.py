## a) Liste definieren

bag = ['key', 20, 50, 10, 50,
          'hairbrush', 'portemonnaie',
          'mirror', 'powder', 'Smartphone']
print(bag)

## b) Prüfen, ob Bürste in Tasche enthalten
if 'hairbrush' in bag:
    print('hairbrush available')
else:
    print('hairbrush not available')

## c) Spiegel entfernen
bag.remove('mirror')   ### del(tasche[7])
print('after remove mirror', bag)

## d) Münzen über Slicing herauslösen [Start: Ende(exkl.)]
coins = bag[1:5]
print('coins:', coins)

## e) Anz. Elemente in Liste
num = len(coins)
print('length list: ', num)

## f) Liste in Set umwandeln
coins_set = set(coins)
print('length set', len(coins_set))