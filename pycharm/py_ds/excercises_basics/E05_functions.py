#### Search Function


def search_in_list_for(a_list, search_string):
    for i in range(len(a_list)):  ### for-loop
        if a_list[i] == search_string:
            return i   ### return index and end the function
    return -1

def search_in_list_for(a_list, search_string):
    for i, el in enumerate(a_list):  ### for-loop
        if el == search_string:
            return i   ### return index and end the function
    return -1

def search_in_list_for(a_list, search_string):
    i = 0
    while i < len(a_list):
        if a_list[i] == search_string:
            return i
        i+=1
    return -1


a_list = ['A', 'B', 'C', 'D', 'B']
return_value = search_in_list_for(a_list, 'U')
print(return_value)