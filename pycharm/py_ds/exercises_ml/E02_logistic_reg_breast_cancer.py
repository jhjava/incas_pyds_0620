from os.path import join
import pandas as pd
import numpy as np

pd.set_option('display.max_columns', 20)
pd.set_option('display.precision', 3)
np.set_printoptions(precision=3, suppress=True)

## a) CSV-Datei als DataFrame laden
path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'breast_cancer_wisconsin.csv'

df = pd.read_csv(join(path, file))
print(df.head())

### b) recode target variable
print(df['label'].value_counts())

df['target'] = df['label'].map({'malignant':1, 'benign':0})
print(pd.crosstab(df['target'], df['label']))

## c) & d) Extraktion der Daten & Train/Test Split
from sklearn.model_selection import train_test_split

X = df.drop(['id', 'label', 'target'], axis=1)
y = df['target']

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    random_state=13)
print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)


### e) Select and train model
from sklearn.linear_model import LogisticRegression

logistic = LogisticRegression()
logistic.fit(X_train, y_train)
print(logistic.coef_)

#### f) Evaluation
## 1) accuracy

accuracy = logistic.score(X_test, y_test)
print('accuracy', accuracy)

## 2) confusion matrix
from sklearn.metrics import confusion_matrix

### Predictions für die Testdaten erzeugen
y_pred_test = logistic.predict(X_test)

matrix = confusion_matrix(y_test, y_pred_test)
print('confusion matrix\n', matrix)

###############################################
###### Preprocessing and Post-Processing ######
###############################################

### Cross Validation:
### train model on different folds of the Data and evaluate it on other folds
### to get a better impression on how it works...
from sklearn.model_selection import cross_val_score
logistic_cv = LogisticRegression()
cv_score = cross_val_score(logistic_cv, X=X, y=y, cv=5)
print(cv_score)
print('Mean score', cv_score.mean(), 'std score', cv_score.std())

############################################
### Post-Processing ########################
############################################
### confusion matrix
print('confusion matrix\n', matrix)

### we want cases to be classified as benign only if prob > 99%
y_test_pred_prob = logistic.predict_proba(X_test)
print(y_test_pred_prob[-10:])


y_pred_classes99b = []
for b_prob, m_prob in y_test_pred_prob:
    if b_prob > .99:
        y_pred_classes99b.append(0)  ## benign
    else:
        y_pred_classes99b.append(1)  ### malignant

y_pred_classes99b[-10:]
new_matrix99b = confusion_matrix(y_test, y_pred_classes99b)
print('confusion matrix\n', new_matrix99b)