import pandas as pd
from os.path import join
from sklearn.linear_model import LinearRegression
import numpy as np
pd.set_option('display.max_columns', 20)
pd.set_option('display.width', 200)
np.set_printoptions(precision=3, suppress=True)

## a) Load the data
path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'kc_house_data.csv'

df = pd.read_csv(join(path, file), sep=',', header=0)
print(df.head(10))

## b) Extract data: X/y-Arrays extrahieren
X = df.drop(['id', 'date', 'price'], axis=1)   ### 2D-Matrix
print(X.corr())

#### standardize data
X_sd = (X - X.mean()) / X.std()
X_sd.mean().round(3)
X_sd.std().round(3)
y = df['price']   ### 1D-Matrix

## c) Instanciate model and train the model
### object of type linear regression
model = LinearRegression()
## train model with data
model.fit(X_sd, y)

## Optional: coefficients and intercept
print('Intercept and weights of variables')
print(model.intercept_, model.coef_)

### Evaluate model:
print('== Evaluation ==')
## R-Square: explained variance (between 0: explains nothing and 1 - perfectly explains target)
r2 = model.score(X, y)
print('r2', r2)

## mean absolute error. error when using model to make predictions:
from sklearn.metrics import mean_absolute_error
y_pred = model.predict(X)  ### predictions with X-variables
mae = mean_absolute_error(y, y_pred)
print('mae', mae)
y.describe().round(3)

# c) Make predictins
X_pred_dat = pd.read_csv(join(path, 'predictions.csv'))
cols = list(X_pred_dat.columns)

y_pred_dat = model.predict(X_pred_dat)
print('== predictions ==')
print(X_pred_dat)
print('Predictions:', y_pred_dat)