
def add_values(value1, value2):
    return value1 + value2

def subtract_values(value1, value2):
    return value1 - value2

### objects consist of attributes (data), methods
class Bottle:
    def __init__(self, content=0):   ### constructor
        self.content = content
        self.color = 'white'
    def fill(self, amount):
        self.content += amount
    def draft(self, amount):
        if self.content >= amount:
            self.content -= amount
            return amount
        print('not enough content')

b1 = Bottle(20)  ### calling a constructor of a class
print(b1.content )   ## accessing an attribute of a class
b1.fill(100)   ### calling a instance method of a object
print(b1.content )
var = b1.draft(50)
print('var', var, 'content', b1.content )
### automatically created reference variable
### __name__ == '__main__' if app is started
### from this file
### otherwise: __name__ is name of packacke.file
print('__name__', __name__)

if __name__ == '__main__':
    var = add_values(7, 8)
    print('test function in arithmetics', var)