### import classes and functions you need
from os.path import join
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# ### Übungsaufgabe: Graphiken mit Seaborn / Pandas

## a) loading the dataframe
path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'health_care_data.csv'

df = pd.read_csv(join(path, file))
print(df.head())

# b) Histogram bmi
sns.distplot( df['bmi'] )
plt.show()

## c)  bmi by worktype / barbplot
sns.catplot(data=df, x='work_type', y='bmi', kind='bar')
plt.ylim(15, 31)
plt.xticks(rotation=45)
plt.tight_layout()
plt.savefig('catplot_health_data.png', dpi=200)
plt.show()

### d) relation between age and bmi by smoking status
sns.relplot(data=df, x='age', y='bmi', kind='line',
            hue='smoking_status', ci=False)
plt.show()

df['work_type'].value_counts()
# ## e)
# ##  relative proportion of persons who smoke (y) for
# # ## different categories of the variable work_type (x).
df['smoking_stat_int'] = df['smoking_status'].map(
             lambda x: 1 if x=='smokes' else 0)

#df['smoking_stat_int'] = df['smoking_status'].map({ 'never smoked':0, 'formerly smoked':0,
 #                                                   'smokes': 1})
pd.crosstab(df['smoking_stat_int'], df['smoking_status'])

sns.catplot(data=df, x='work_type', y='smoking_stat_int', kind='bar')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()

# #### Alternative Solution: Stack diagram
# ### 1) first create crosstab with data prepared for diagram
cws = pd.crosstab(df['work_type'], df['smoking_status'], normalize='index')
cws['formerly_never'] = cws['formerly smoked'] + cws['smokes']
cws['total'] = cws['formerly smoked'] + cws['never smoked'] + cws['smokes']
cws
#
# ### 2) second create barplots with overlapping bars
# ## background
sns.barplot(data=cws, x=cws.index, y='total', color='blue', label='never smoked')
# ## foreground 1
sns.barplot(data=cws, x=cws.index, y='formerly_never', color='red', label='formerly smoked')
# ## foreground 2
sns.barplot(data=cws, x=cws.index, y='smokes', color='green', label='smoke')
plt.legend()
plt.ylabel('prop.')
plt.tight_layout()