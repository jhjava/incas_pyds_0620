from os.path import join
import pandas as pd

path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'health_care_data.csv'

pd.set_option('display.max_columns', 20)
pd.set_option('display.float_format', lambda x: '%.3f' % x)

df = pd.read_csv( join(path, file), sep=',', header=0 )
print(df.head(5))

## b) Frequency-table work_type
print(df.columns)
print(df['work_type'].value_counts(normalize=True) )

## c) crosstab: Heart disease / work_type
crosstab = pd.crosstab( df['work_type'],
            df['heart_disease'], normalize='index')
print(crosstab)

### d) Recoding
## Variante 1 with lambda
df['gender_int'] = df['gender'].map(lambda x: 1 if x== 'Female' else 0)

### Variante 2 with dictionary
df['gender_int'] = df['gender'].map({'Female':1, 'Male':0, 'Other':0})
print(pd.crosstab(df['gender_int'], df['gender']))

### e) mean / Std for bmi and age
age_mean, age_std = df['age'].mean(), df['age'].std()
bmi_mean, bmi_std = df['bmi'].mean(), df['bmi'].std()
print(age_mean, age_std, bmi_mean, bmi_std)

### f) Means over age and  glucose level
df['hypertension'].value_counts()
print(df.groupby(['hypertension'])[['age', 'avg_glucose_level']].mean())

### g) Standardize age:
age_mean, age_std = df['age'].mean(), df['age'].std()

df['age_std'] = (df['age'] - age_mean) / age_std
df['age_std'] = (df['age'] - df['age'].mean()) / df['age'].std()

print(df['age_std'].mean().round(3), df['age_std'].std().round(3))

