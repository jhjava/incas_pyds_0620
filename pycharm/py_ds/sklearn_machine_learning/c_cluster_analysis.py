from os.path import join
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'iris_data.csv'

pd.set_option('display.max_columns', 20)
pd.set_option('display.float_format', lambda x: '%.3f' % x)

df = pd.read_csv( join(path, file), sep=',', header=0 )
print(df.head(5))

#####
# 1. Select features
X = df.drop(['name_target', 'target'], axis=1)
X.head()

## 2. Standardize your data before perfomin KMeans

## 3. Train the KMeans
from sklearn.cluster import KMeans
kmeans = KMeans(n_clusters=2)
kmeans.fit(X)
print('heterogentiy', kmeans.inertia_)

## 4. Assign to every row in data corresponding Cluster
df['cluster'] = kmeans.predict(X)
df['cluster'].value_counts()

import seaborn as sns
sns.relplot(data=df, x='petal length (cm)', y='petal width (cm)',
            hue='cluster')
df.groupby(['cluster'])[['sepal length (cm)',  'sepal width (cm)',
                       'petal length (cm)',  'petal width (cm)']].mean()

#### how to get the best result of cluster analysis
range_cluster = [1, 2, 3, 4, 5, 6, 7, 8]
inertias = []
for n in range_cluster:
    kmeans = KMeans(n_clusters=n)
    kmeans.fit(X)
    inertias.append( kmeans.inertia_)

import matplotlib.pyplot as plt
plt.plot(range_cluster, inertias)
plt.show()