import pandas as pd
from os.path import join
import numpy as np
import seaborn as sns
pd.set_option('display.max_columns', 20)
pd.set_option('display.width', 200)
np.set_printoptions(precision=3, suppress=True)

## a) Load the data
path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'iris_data.csv'

df = pd.read_csv(join(path, file), sep=',', header=0)
print(df.head(10))

#### Scatterplot with petal length and petal length and iris
sns.relplot(data=df, x='petal length (cm)', y='petal width (cm)', hue='name_target')

###################################################
##### Workflow ####################################
###################################################
# a) Separate X / y-data
# b) Train / Test-Split (Divide sample in training and evaluation sample)
# c) Select model and train (train_partition)
# d) Evaluate model on test partition
# --> prediction

# a) Separate X / y-data
X = df.drop(['target', 'name_target'], axis=1)
y = df['target']
#### check how target variable is encoded (to interpret results)
print(X.shape)
print(pd.crosstab(df['target'], df['name_target']))

# b) Make train / test split
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=11)
print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

# c) Select model and train model

from sklearn.linear_model import LogisticRegression
## binary_logistic = LogisticRegression() ## logistic regression with 0/1 as target
softmax = LogisticRegression(multi_class='multinomial')

softmax.fit(X_train, y_train)

### check coef_ of model
print('coef.:\n', softmax.coef_)

# d) Evaluate model on test partition
#### accuracy of the model. Correct classifications in comparision to all cases
accuracy = softmax.score(X_test, y_test)
print('accuracy', accuracy)

#### confusion_matrix
from sklearn.metrics import confusion_matrix, classification_report

y_test_pred = softmax.predict(X_test)
print(y_test_pred[:5])
matrix = confusion_matrix(y_test, y_test_pred)
print('confusion matrix')
print(matrix)

report = classification_report(y_test, y_test_pred)
print('classification report (recall, precision etc.')
print(report)

#################### make prediction: predict / predict_proba
X_pred = df.iloc[77:78, 0:4].values
X_pred = df.iloc[78:79, 0:4].values
X_pred
y_pred_2 = softmax.predict(X_pred)   ### returns classes
y_pred_prob_2 = softmax.predict_proba(X_pred)  ### returns probability for each class
print(y_pred_prob_2)
