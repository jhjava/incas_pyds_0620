from os.path import join
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

path = r'C:\Users\jhjav\git\incas_pyds_0620\data'
file = 'insurance.csv'

pd.set_option('display.max_columns', 20)
pd.set_option('display.float_format', lambda x: '%.3f' % x)

df = pd.read_csv( join(path, file), sep=',', header=0 )
print(df.head(5))

##### Linear Regression using scikit-learn
### ...to model the relation between age and charges

sns.relplot(data=df, x='age', y='charges')
plt.show()

#### Train a model wit scikitlearn
#### 1) Extract X / y-data (features / target-variable)
####    .... in differen array

X = df[['age']].values   ### values produces a numpy array out of df/series
                         ####  Feature variables should always be provide in 2d-array
y = df['charges'].values

print(X[:5], y[:5])

#### 2) Instanciate and train Linear Model
from sklearn.linear_model import LinearRegression
model = LinearRegression()
model.fit(X, y)

### check the coefficients (only possible after training)
print('intercept:', model.intercept_, 'coef.:', model.coef_)

#### 3) Predictions
y_pred = model.predict([[18], [65]])
print(y_pred)

### model in scatterplot
sns.relplot(data=df, x='age', y='charges')
plt.plot([[18], [65]], y_pred)

### 4) Evaluate the performance of a model: r_square / mean absolute error
r_square = model.score(X, y)
print('r_square', r_square)

from sklearn.metrics import mean_absolute_error
y_pred = model.predict(X)
y_true = y
mae = mean_absolute_error(y_true, y_pred)
print('mean absolute error', mae)


#### Multiple Linear Regression
X = df[['age', 'bmi', 'children']].values
y = df['charges'].values

linear = LinearRegression()
linear.fit(X, y)
print('coef', linear.coef_)
print('r_square', linear.score(X, y))
